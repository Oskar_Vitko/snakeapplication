﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SnakeRework
{
    class Draw
    {
        public static void FunctionDraw()
        {
            bool c = false;
            Console.Clear();
            for (int i = 0; i < Program.Width; i++)
            {
                Console.Write("#");
            }
            for (int i = 0; i < Program.Height; i++)
            {
                Console.WriteLine();
                Console.Write("#");
                for (int j = 0; j < Program.Width - 2; j++)
                {
                    if (i + 1 == Program.SnakeY && j + 1 == Program.SnakeX) Console.Write("O");
                    else
                    if (i + 1 == Program.EatY && j + 1 == Program.EatX) Console.Write("$");
                    else if (i + 1 == Program.BonusY && j + 1 == Program.BonusX) Console.Write("@");
                    else
                    {
                        for (int k = 0; k < Program.hvost.Count; k++)
                        {
                            c = false;
                            if (i + 1 == Program.hvost[k].Y && j + 1 == Program.hvost[k].X) { Console.Write("o"); c = true; break; }
                        }
                        if (c == false) Console.Write(" ");
                    }
                }
                Console.Write("#");
            }
            Console.WriteLine();
            for (int i = 0; i < Program.Width; i++)
            {
                Console.Write("#");
            }
            Console.WriteLine($"\nКолчество очков: {Program.Score}");
        }
    }
}
