﻿using System;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Generic;


namespace SnakeRework
{
   public class Hvost
    {
        public int X;
        public int Y;
        public Hvost(int _x, int _y)
        {
            X = _x;
            Y = _y;
        }
    }
    public class Program
    {

        public static bool potok = true;
        public static int level = 100;
        public static int Width = 60;
        public static int Height = 20;
        public static int SnakeX = 19, SnakeY = 9, EatX = 5, EatY = 5, Score = 0, BonusX = 0, BonusY = 0;
        public static List<Hvost> hvost = new List<Hvost>();
        public enum Direction
        {
            Stop,
            Up,
            Right,
            Left,
            Down
        }
        static Direction direction;
        static void Main(string[] args)
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1. Играть\n2. Выбрать уровень сложности\n3. Выйти из игры");
                switch (Console.ReadLine())
                {
                    case "1": Game(); break;
                    case "2": GameLevel(); ResetData(); break;
                    case "3": return;
                    default: Console.WriteLine("Ошибка! Повторите 1-3!"); break;
                }
            }
        }

        static void GameLevel()
        {
            while (true)
            {
                Console.Clear();
                Console.WriteLine("1. Лёгкий\n2. Средний\n3. Тяжёлый");
                switch (Console.ReadLine())
                {
                    case "1":
                        level = 200;
                        Width = 60;
                        return;
                    case "2":
                        level = 100;
                        Width = 60;
                        return;
                    case "3":
                        level = 50;
                        Width = 40;
                        return;
                    default: Console.WriteLine("Ошибка! Повторите 1-3!"); break;
                }
            }
        }
        static void ResetData()
        {
            Score = 0;
            hvost.Clear();
            potok = false;
            if (level == 50)
                SnakeX = 19;
            else if (level == 100)
                SnakeX = 29;
            else SnakeX = 29;
            SnakeY = 9;
            hvost.Add(new Hvost(SnakeX, SnakeY + 1));
            hvost.Add(new Hvost(SnakeX, SnakeY + 2));
            SpawnEat();
            BonusX = 0;
            BonusY = 0;
        }
        static void Game()
        {
            ResetData();
            potok = true;
            Task task = Task.Factory.StartNew(SnakeDirection);
            while (true)
            {
                Draw.FunctionDraw();
                if (SnakeDie()) break;
                Eat();
                SnakeMove();
                Thread.Sleep(level);
            }
            ResetData();
            Console.ReadLine();
        }

        static void Eat()
        {
            if (SnakeX == EatX && SnakeY == EatY)
            {
                Score += 1;
                SpawnEat();
                hvost.Add(new Hvost(SnakeX, SnakeY));
                if (Score % 5 == 0)
                {
                    SpawnBonus();
                    Thread TimerBonus = new Thread(DeleteBonus);
                    TimerBonus.Start();
                }
            }
            if (SnakeX == BonusX && SnakeY == BonusY)
            {
                Score += 6;
                BonusX = 0;
                BonusY = 0;
                hvost.Add(new Hvost(SnakeX, SnakeY));
            }
        }
        static void DeleteBonus()
        {
            Thread.Sleep(5000);
            BonusX = 0;
            BonusY = 0;
        }
        static bool SnakeDie()
        {
            if (SnakeX == 0 || SnakeY == 0 || SnakeX == Width - 1 || SnakeY == Height + 1)
            {
                Console.Clear();
                direction = Direction.Stop;
                Console.WriteLine($"Вы проиграли\nНабранное количество очков: {Score}");
                return true;
            }
            for (int i = 0; i < hvost.Count; i++)
                if (SnakeX == hvost[i].X && SnakeY == hvost[i].Y)
                {
                    Console.Clear();
                    direction = Direction.Stop;
                    Console.WriteLine($"Вы проиграли\nНабранное количество очков: {Score}");
                    return true;
                }
            return false;
        }
        static void SnakeMove()
        {
            if (hvost.Count > 1 && direction != Direction.Stop)
                for (int i = hvost.Count - 1; i > 0; i--)
                {
                    hvost[i].X = hvost[i - 1].X;
                    hvost[i].Y = hvost[i - 1].Y;
                }
            if (hvost.Count > 0 && direction != Direction.Stop)
            {
                hvost[0].X = SnakeX;
                hvost[0].Y = SnakeY;
            }
            if (direction == Direction.Up)
                SnakeY -= 1;
            if (direction == Direction.Down)
                SnakeY += 1;
            if (direction == Direction.Right)
                SnakeX += 1;
            if (direction == Direction.Left)
                SnakeX -= 1;
        }
        static void SnakeDirection()
        {
            while (potok)
                switch (Console.ReadKey().KeyChar)
                {
                    case 'w':
                        if (direction != Direction.Down)
                            direction = Direction.Up;
                        break;
                    case 's':
                        if (direction != Direction.Up && direction != Direction.Stop)
                            direction = Direction.Down;
                        break;
                    case 'a':
                        if (direction != Direction.Right)
                            direction = Direction.Left;
                        break;
                    case 'd':
                        if (direction != Direction.Left)
                            direction = Direction.Right;
                        break;
                    case 'ц':
                        if (direction != Direction.Down)
                            direction = Direction.Up;
                        break;
                    case 'ы':
                        if (direction != Direction.Up && direction != Direction.Stop)
                            direction = Direction.Down;
                        break;
                    case 'ф':
                        if (direction != Direction.Right)
                            direction = Direction.Left;
                        break;
                    case 'в':
                        if (direction != Direction.Left)
                            direction = Direction.Right;
                        break;
                }
        }
       
        static void SpawnEat()
        {
            bool tr = false;
            while (true)
            {
                int NewEatX = new Random().Next(2, Width - 2);
                int NewEatY = new Random().Next(2, Height - 2);

                if (NewEatX < 2 || NewEatX == SnakeX || NewEatY < 2 || NewEatY == SnakeY) continue;
                else
                {
                    for (int i = 0; i < hvost.Count; i++)
                    {
                        tr = false;
                        if (NewEatY == hvost[i].Y && NewEatX == hvost[i].X) { tr = true; break; }
                    }
                    if (tr == false) { EatX = NewEatX; EatY = NewEatY; break; }
                    else continue;
                }
            }
        }
        static void SpawnBonus()
        {
            bool tr = false;
            while (true)
            {
                int NewBonusX = new Random().Next(2, Width - 2);
                int NewBonusY = new Random().Next(2, Height - 2);

                if (NewBonusX < 2 || NewBonusX == SnakeX || NewBonusY < 2 || NewBonusY == SnakeY || NewBonusX == EatX || NewBonusY == EatY) continue;
                else
                {
                    for (int i = 0; i < hvost.Count; i++)
                    {
                        tr = false;
                        if (NewBonusY == hvost[i].Y && NewBonusX == hvost[i].X) { tr = true; break; }
                    }
                    if (tr == false) { BonusX = NewBonusX; BonusY = NewBonusY; break; }
                    else continue;
                }
            }
        }
    }
}
